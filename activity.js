function oddEvenChecker(num){
	
	if(typeof num === "number"){
		if(num%2 === 1){
			console.log("The number is odd.");
		} else{
			console.log("The number is even.");
		}
	} else {
		alert("Invalid input.")
	}	
}

function budgetChecker(budget){
	if(typeof budget === "number"){
		if(budget>40000){
			console.log("You are over the budget.");
		} else{
			console.log("You have resources left.");
		}
	} else {
		console.log("Invalid input.")
	}	
}
